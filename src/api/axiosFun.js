import axios from 'axios';

// 登录请求方法
const loginreq = (method, url, params) => {
    return axios({
        method: method,
        url: url,
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Type': 'application/json',
        },
        data: params,
        traditional: true,
        transformRequest: [
            function(data) {
                let ret = ''
                for (let it in data) {
                    ret +=
                        encodeURIComponent(it) +
                        '=' +
                        encodeURIComponent(data[it]) +
                        '&'
                }
                return ret
            }
        ]
    }).then(res => res.data);
};
// 通用公用方法
const req = (method, url, params) => {
    return axios({
        method: method,
        url: url,
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Type': 'application/json',
            // token: localStorage.getItem('logintoken')
        },
        // data: params,
      data:params,
        traditional: true,
        transformRequest: [
            function(data) {
                return data
            }
        ]
    }).then(res => res.data);
};

const reqUpload = (method, url, params) => {
  return axios({
    method: method,
    url: url,
    headers: {
      // 'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Type':'multipart/form-data; boundary=<calculated when request is sent>',
      // token: localStorage.getItem('logintoken')
    },
    // data: params,
    data:params,
    traditional: true,
    transformRequest: [
      function(data) {
        return data
      }
    ]
  }).then(res => res.data);
};

export {
    loginreq,
    req,
  reqUpload
}
